package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    ListView listAnimal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final ListView listAnimal= (ListView) findViewById(R.id.listAnimal);

        final String[] nameArray = {"Renard roux","Koala","Montbéliarde","Panda géant","Ours brun","Chameau", "Lion"};

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, nameArray);
        listAnimal.setAdapter(adapter);

        listAnimal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //setContentView(R.layout.activity_animal);
                Intent intent = new Intent(MainActivity.this, AnimalActivity.class);
                intent.putExtra("name",nameArray[position]);
                startActivity(intent);
            }
        });
    }
}
