package com.example.tp1;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageSwitcher;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ImageView;

import org.w3c.dom.Text;

public class AnimalActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        //Récupérer l'extra information de l'intent
        String animalName = getIntent().getExtras().getString("name");

        final Animal animal = AnimalList.getAnimal(animalName);

        //Nom de l'animal
        final TextView textAnimalName = (TextView) findViewById(R.id.textView3);
        textAnimalName.setText(animalName);

        //Image de l'animal
        final ImageView imageAnimal = (ImageView) findViewById(R.id.animalImg);
        Resources res = getResources();
        String drawableName = animal.getImgFile();
        int resID = res.getIdentifier(drawableName, "drawable", getPackageName());
        Drawable drawable = res.getDrawable(resID);
        imageAnimal.setImageDrawable(drawable);

        //Espérance de vie max
        final TextView textEspMax= (TextView) findViewById(R.id.textViewLifeSpan);
        textEspMax.setText(animal.getStrHightestLifespan());

        //Période de gestation
        final TextView textGest= (TextView) findViewById(R.id.textViewGestationPeriod);
        textGest.setText(animal.getStrGestationPeriod());

        //Poids à la naissance
        final TextView textBirth= (TextView) findViewById(R.id.textViewBirthWeight);
        textBirth.setText(animal.getStrBirthWeight());

        //Poids à l'âge adulte
        final TextView textAdult= (TextView) findViewById(R.id.textViewAdultWeight);
        textAdult.setText(animal.getStrAdultWeight());

        //Statut de conservation

        final EditText textStatus = (EditText) findViewById(R.id.editTextStatut);
        textStatus.setText(animal.getConservationStatus());
        final Button saveB = (Button) findViewById(R.id.saveButton);
        saveB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cStatus = textStatus.getText().toString();
                animal.setConservationStatus(cStatus);
            }
        });
    }
}
