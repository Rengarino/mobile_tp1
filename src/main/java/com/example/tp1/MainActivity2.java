package com.example.tp1;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {

    final String[] items = AnimalList.getNameArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        final RecyclerView rv = (RecyclerView) findViewById(R.id.recyclerView);

        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(new IconicAdapter());
    }


    class IconicAdapter extends RecyclerView.Adapter<RowHolder> {
        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return (new RowHolder(getLayoutInflater()
                    .inflate(R.layout.row, parent, false)));
        }

        @Override
        public void onBindViewHolder(RowHolder holder, int position) {
            holder.bindModel(items[position]);
        }

        @Override
        public int getItemCount() {
            return(items.length);
        }
    }

    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView label = null;
        ImageView icon = null;

        RowHolder(View row) {
            super(row);
            label=(TextView)row.findViewById(R.id.label);
            icon=(ImageView)row.findViewById(R.id.icon);
            row.setOnClickListener(this); //Waiting for a click
        }

        @Override
        public void onClick(View v) { //On click switching view and giving infos
            final String item = label.getText().toString();
            Intent intent = new Intent(MainActivity2.this, AnimalActivity.class);
            intent.putExtra("name", item);
            startActivity(intent);
        }

        void bindModel(String item) {
            label.setText(item);

            final Animal animal = AnimalList.getAnimal(item);
            Resources res = getResources();
            String drawableName = animal.getImgFile();
            int resID = res.getIdentifier(drawableName, "drawable", getPackageName());
            Drawable drawable = res.getDrawable(resID);
            icon.setImageDrawable(drawable);
        }
    }
}